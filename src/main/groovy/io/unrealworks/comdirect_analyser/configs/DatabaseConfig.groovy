package io.unrealworks.comdirect_analyser.configs

import groovy.sql.Sql
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration

import javax.sql.DataSource

@Configuration
class DatabaseConfig {

    @Bean
    Sql sql(DataSource dataSource){
        return new Sql(dataSource);
    }
}
