package io.unrealworks.comdirect_analyser.features

import groovy.sql.GroovyResultSet
import groovy.sql.Sql
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import org.springframework.stereotype.Component

@Component("transaction-costs-value-extractor")
class TransactionCostsValueExtractor implements Feature<String, Integer> {
    private final Logger log = LoggerFactory.getLogger(TransactionCostsValueExtractor)

    private final Sql sql;

    TransactionCostsValueExtractor(Sql sql) {
        this.sql = sql
    }

    @Override
    Integer compute(String param) {
        int appliedUpdates = 0;
        def knownIds = selectKnownIds()
        this.sql.eachRow('select rd.id, rd.content from raw_data rd') { row ->
            try {
                def id = row.id
                if (knownIds[id] == null) {
                    log.debug('found data for id {}', id)
                    def transactionValue = getTransactionValue(row)
                    transactionValue.id = id;
                    log.debug("transaction value is {}", transactionValue);
                    updateTransaction(transactionValue, appliedUpdates)
                }
            } catch (e) {
                log.error("", e);
            }
        }
        return appliedUpdates
    }

    private void updateTransaction(def transaction_costs, int appliedUpdates) {
        this.sql.executeUpdate('''update extracted_data set provision=:provision, clear_stream_provision=:clear_stream_provision, 
                                    exchange_price=:exchange_price, wages_sum=:wages_sum, provisions_sum=:provisions_sum where id=:id''', transaction_costs)
        appliedUpdates++
    }

    private def getTransactionValue(GroovyResultSet row) {
        def result = [:]
        def marker = -1;

        row.content.split('\n').each({ line ->
            def split = line.split()
            if(line.contains('Provision')){
                result.provision = split.last()
            }
            if(line.contains('Abwickl.entgelt Clearstream')){
                result.clear_stream_provision = split.last()
            }
            if(line.contains('Börsenplatzabhäng. Entgelt')){
                result.exchange_price = split.last()
            }
            if (line.contains('Gesamtprovision')) {
                println 'Gesamtprovision'
                result.wages_sum = split.last()
            }
            if(line.contains('Summe Entgelte')){
                result.provisions_sum = split.last()
            }

            if(marker>3){
                throw new Exception('failed to find data for shares transaction info');
            }
            if(line.contains('Eigene Entgelte')){
                println 'Eigene Entgelte'
                marker++;
            }
        })
        return result;
    }

    private def selectKnownIds() {
        def result = [:]
        this.sql.eachRow('select id, number_of_shares from extracted_data') { row ->
            result[row.id] = row.number_of_shares
        }
        return result;
    }
}
