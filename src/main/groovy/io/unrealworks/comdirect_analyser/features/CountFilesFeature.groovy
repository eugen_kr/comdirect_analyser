package io.unrealworks.comdirect_analyser.features

import org.springframework.stereotype.Component

@Component("files-counter")
class CountFilesFeature implements Feature<String,Integer> {

    Integer compute(final String path){
        final File f = new File(path);
        if(f.exists() && f.listFiles()!=null) {
            return f.listFiles().length;
        }else{
            return -1;
        }
    }
}
