alter table extracted_data add column execution_date_as_date date;

update extracted_data set execution_date_as_date = STR_TO_DATE(execution_date, '%d.%m.%Y') where true;

alter table extracted_data add column wkn varchar(50);
alter table extracted_data add column shares_name varchar(255);
alter table extracted_data add column isin varchar(255);