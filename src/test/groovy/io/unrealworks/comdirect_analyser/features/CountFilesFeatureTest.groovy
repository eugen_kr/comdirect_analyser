package io.unrealworks.comdirect_analyser.features;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class CountFilesFeatureTest {

    private final CountFilesFeature fut = new CountFilesFeature();

    @Test
    void testCompute() {
        assertEquals(2, fut.compute("./src/test/resources/test-directory"));
    }

    @Test
    void testComputeWithFaultyPath() {
        fut.compute("xyb/test");
    }
}