package io.unrealworks.comdirect_analyser.features

import groovy.sql.GroovyResultSet
import groovy.sql.Sql
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import org.springframework.stereotype.Component

@Component("transaction-type-extractor")
class TransactionTypeExtractor implements Feature<String, Integer> {
    private final Logger log = LoggerFactory.getLogger(TransactionTypeExtractor)

    private final Sql sql;

    TransactionTypeExtractor(Sql sql) {
        this.sql = sql
    }

    @Override
    Integer compute(String param) {
        int appliedUpdates = 0;
        def knownIds = selectKnownIds()
        this.sql.eachRow('select rd.id, rd.content from raw_data rd') { row ->
            def id = row.id
            if (knownIds[id] == null) {
                log.debug('found data for id {}', id)
                def ttype = getTransactionType(row);
                updateTransaction(ttype, id, appliedUpdates)
            } else {
                log.info("id {} is already processed", id)
            }
        }
        return appliedUpdates
    }

    private void updateTransaction(String ttype, def id, int appliedUpdates) {
        log.info("updating transaction to type {}", ttype)
        if (ttype != '') {
            this.sql.executeUpdate('update extracted_data set transction_type=:ttype where id=:id', ttype: ttype, id: id)
            appliedUpdates++
            return
        } else {
            log.info("no transaction type for id {} found", id)
        }
    }

    private String getTransactionType(GroovyResultSet row) {
        def result = ''
        row.content.split('\n').each({ line ->
            if (line.startsWith('Wertpapierkauf')) {
                result = 'buy';
                return
            }
            if (line.startsWith('Wertpapierverkauf')) {
                result = 'sell'
                return
            }
        })
        return result;
    }

    private def selectKnownIds() {
        def result = [:]
        this.sql.eachRow('select id, transction_type from extracted_data') { row ->
            result[row.id] = row.transction_type
        }
        return result;
    }
}
