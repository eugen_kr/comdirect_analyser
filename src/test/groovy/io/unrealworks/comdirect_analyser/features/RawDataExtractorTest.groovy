package io.unrealworks.comdirect_analyser.features

import groovy.sql.Sql
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.extension.ExtendWith
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.test.context.junit.jupiter.SpringExtension;
import static org.junit.jupiter.api.Assertions.*

@ExtendWith(SpringExtension)
@SpringBootTest
class RawDataExtractorTest {

    @Autowired
    private RawDataExtractor fut;

    @Autowired
    private Sql sql;

    @Test
    void test_compute() {
        fut.compute("./src/test/resources/test-directory")
        assertEquals(2, this.sql.firstRow('select count(*) as count from raw_data').count)
    }
}