package io.unrealworks.comdirect_analyser.features

import org.junit.jupiter.api.Test

import static org.junit.jupiter.api.Assertions.assertNull
import static org.junit.jupiter.api.Assertions.assertEquals

class TestAttributes {

    @Test
    void testDynamicAttributs(){
        def testObj = [:]

        testObj.'est' = 1;

        testObj.each {k,v->
            println k
            println v
        }

        assertNull(testObj.'test')
        assertEquals(1, testObj.'est')
    }
}
