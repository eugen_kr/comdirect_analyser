package io.unrealworks.comdirect_analyser.features

import groovy.sql.Sql
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import org.springframework.stereotype.Component

@Component("execution-date-extractor")
class TransactionExecutionDateExtractor implements Feature<String, Integer> {
    private final Logger log = LoggerFactory.getLogger(TransactionExecutionDateExtractor)

    private final Sql sql;

    TransactionExecutionDateExtractor(Sql sql) {
        this.sql = sql
    }

    @Override
    Integer compute(String param) {
        int appliedUpdates = 0;
        def knownIds = selectKnownIds()
        this.sql.eachRow('select rd.id, rd.content from raw_data rd') { row ->
            def id = row.id
            if (knownIds[id] == null) {
                log.debug('found data for id {}', id)

                def text = row.content
                text.split('\n').each({ line ->
                    if (line.contains('ABRECHNUNG VOM ') || line.contains('Geschäftstag : ')) {
                        this.sql.execute 'insert into extracted_data(id, execution_date) values (?,?)', [id, line.split()[2]]
                        appliedUpdates++
                    }
                })
            } else {
                log.info("id {} is already processed", id)
            }
        }
        this.sql.executeUpdate( 'update extracted_data set execution_date_as_date = STR_TO_DATE(execution_date, \'%d.%m.%Y\') where true;')
        return appliedUpdates
    }

    private def selectKnownIds() {
        def result = [:]
        this.sql.eachRow('select id from extracted_data') { row ->
            result[row.id] = row.id
        }
        return result;
    }
}
