alter table extracted_data add column provision varchar(255);
alter table extracted_data add column clear_stream_provision varchar(255);
alter table extracted_data add column exchange_price varchar(255);
alter table extracted_data add column wages_sum varchar(255);
alter table extracted_data add column provisions_sum varchar(255);