package io.unrealworks.comdirect_analyser

import org.junit.Test
import org.junit.runner.RunWith
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.test.context.junit4.SpringRunner

@RunWith(SpringRunner)
@SpringBootTest
class ComdirectAnalyserApplicationTests {

	@Test
	void contextLoads() {
	}

}
