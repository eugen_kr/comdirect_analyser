package io.unrealworks.comdirect_analyser.features

import groovy.sql.GroovyResultSet
import groovy.sql.Sql
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import org.springframework.stereotype.Component


@Component("final-transaction-value-extractor")
class FinalTransactionValue implements Feature<String, Integer> {
    private final Logger log = LoggerFactory.getLogger(FinalTransactionValue)
    private final Sql sql;

    FinalTransactionValue(Sql sql) {
        this.sql = sql
    }

    @Override
    Integer compute(String param) {
        int appliedUpdates = 0;
        def knownIds = selectKnownIds()
        this.sql.eachRow('select rd.id, rd.content from raw_data rd') { row ->
            def id = row.id
            if (knownIds[id] == null) {
                log.debug('found data for id {}', id)
                def ttype = getFinalTransactionValue(row);
                ttype.id = id;
                updateTransaction(ttype)
                appliedUpdates++
            } else {
                log.info("id {} is already processed", id)
            }
        }
        return appliedUpdates
    }

    def getFinalTransactionValue(GroovyResultSet row) {
        def result = [:]

        def handler = null;
        row.content.split('\n').each({ line ->
            if (handler != null) {
                result = handler(line)
                handler = null;
            }
            if ((line.contains('IBAN') || line.contains('Verrechnung über Konto') )&& line.contains('Valuta') && line.contains('Zu Ihren')) {
                handler = finalExpenseHandler
            }
        })

        return result
    }

    def finalExpenseHandler = { line ->
        def result = [:]
        def split = line.split()

        if (split.length > 0) {
            result.final_expenses = split.last()
        }
        return result
    };

    private void updateTransaction(def expenses) {
        this.sql.executeUpdate('update extracted_data set final_expenses=:final_expenses  where id=:id', expenses)
    }

    private def selectKnownIds() {
        def result = [:]
        this.sql.eachRow('select id from extracted_data where final_expenses IS NOT NULL') { row ->
            result[row.id] = row.id
        }
        return result;
    }
}
