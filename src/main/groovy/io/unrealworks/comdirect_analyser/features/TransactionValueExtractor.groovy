package io.unrealworks.comdirect_analyser.features

import groovy.sql.GroovyResultSet
import groovy.sql.Sql
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import org.springframework.stereotype.Component

@Component("transaction-value-extractor")
class TransactionValueExtractor implements Feature<String, Integer> {
    private final Logger log = LoggerFactory.getLogger(TransactionValueExtractor)

    private final Sql sql;

    TransactionValueExtractor(Sql sql) {
        this.sql = sql
    }

    @Override
    Integer compute(String param) {
        int appliedUpdates = 0;
        def knownIds = selectKnownIds()
        this.sql.eachRow('select rd.id, rd.content from raw_data rd') { row ->
            try {
                def id = row.id
                if (knownIds[id] == null) {
                    log.debug('found data for id {}', id)
                    def transactionValue = getTransactionValue(row)
                    transactionValue.id = id;
                    updateTransaction(transactionValue, appliedUpdates)
                } else {
                    log.info("id {} is already processed", id)
                }
            } catch (e) {
                log.error("", e);
            }
        }
        return appliedUpdates
    }

    private void updateTransaction(def sharesInfo, int appliedUpdates) {
        this.sql.executeUpdate('update extracted_data set number_of_shares=:number_of_shares, shares_price=:shares_price, trade_value=:trade_value, currency=:currency where id=:id', sharesInfo)
        appliedUpdates++
    }

    private def getTransactionValue(GroovyResultSet row) {
        def result = [:]

        def handler = {line->return [:]};
        row.content.split('\n').each({ line ->
            if (line.startsWith('Nennwert')) {
                handler = find_number_of_shares
            }
            if (line.startsWith('Handelszeit')) {
                handler = find_transactionDataOverMultipleLine
            }
            if (result.isEmpty()) {
                result = handler(line)
            } else {
                handler = finde_kurzswert_line

                result.leftShift(handler(line))
            }
        });

        return result
    }

    def find_transactionDataOverMultipleLine = { line ->
        def result = [:];
        def split = line.split()
        if (split.length>0 && split[0] == 'Summe') {
            result.number_of_shares = split[2]
            result.shares_price = split[4]
            result.currency = split[5]
            result.trade_value = split[6]
        }
        result
    }

    def finde_kurzswert_line = { line ->
        def result = [:]
        if (line.contains('Kurswert')) {
            result.trade_value = line.split().last()
        }
        result
    }

    def find_number_of_shares = { line ->
        def result = [:]
        if (line.startsWith('St.')) {
            def split = line.split()

            result.number_of_shares = split[1]
            result.shares_price = split[3]
            result.currency = split[2]
        }
        result
    }


    private def selectKnownIds() {
        def result = [:]
        this.sql.eachRow('select id, number_of_shares from extracted_data') { row ->
            result[row.id] = row.number_of_shares
        }
        return result;
    }

}
