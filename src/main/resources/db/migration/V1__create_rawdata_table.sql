create table raw_data
(
    id int not null primary key,
    file_name varchar(255),
    content text
);

create table extracted_data
(
    id             int not null primary key,
    execution_date varchar(255)
);
