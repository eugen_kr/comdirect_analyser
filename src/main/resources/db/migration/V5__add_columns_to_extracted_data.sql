alter table extracted_data add column number_of_shares varchar(50);
alter table extracted_data add column shares_price varchar(255);
alter table extracted_data add column currency varchar(255);
alter table extracted_data add column trade_value varchar(255);