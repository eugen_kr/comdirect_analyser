package io.unrealworks.comdirect_analyser.features

interface Feature<P, R> {
    R compute(P param);
}