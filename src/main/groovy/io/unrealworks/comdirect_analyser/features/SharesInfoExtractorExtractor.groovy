package io.unrealworks.comdirect_analyser.features

import groovy.sql.GroovyResultSet
import groovy.sql.Sql
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import org.springframework.stereotype.Component

@Component("shares-info-extractor")
class SharesInfoExtractorExtractor implements Feature<String, Integer> {
    private final Logger log = LoggerFactory.getLogger(SharesInfoExtractorExtractor)

    private final Sql sql;

    SharesInfoExtractorExtractor(Sql sql) {
        this.sql = sql
    }

    @Override
    Integer compute(String param) {
        int appliedUpdates = 0;
        def knownIds = selectKnownIds()
        this.sql.eachRow('select rd.id, rd.content from raw_data rd') { row ->
            def id = row.id
            if (knownIds[id] == null) {
                log.debug('found data for id {}', id)
                def sharesInfo = getSharesInfo(row)
                sharesInfo.id = id;
                log.debug("shares info is {}", sharesInfo);
                updateTransaction(sharesInfo, appliedUpdates)
            } else {
                log.info("id {} is already processed", id)
            }
        }
        return appliedUpdates
    }

    private void updateTransaction(def sharesInfo, int appliedUpdates) {
        this.sql.executeUpdate('update extracted_data set shares_name=:shares_name, wkn=:wkn, isin=:isin where id=:id', sharesInfo)
        appliedUpdates++
    }

    private def getSharesInfo(GroovyResultSet row) {
        def result = [:]
        def marker = -1;
        row.content.split('\n').each({ line ->
            if (marker == 1) {
                result.isin = line.split().last()
                marker = -1;
                return;
            }
            if (marker == 0) {
                def split = line.split()
                result.wkn = split.last()
                result.shares_name = line - result.wkn
                result.shares_name.trim()
                marker++;
            }
            if (line.startsWith('Wertpapier-Bezeichnung')) {
                marker = 0
            }
        })
        return result;
    }

    private def selectKnownIds() {
        def result = [:]
        this.sql.eachRow('select id, wkn from extracted_data') { row ->
            result[row.id] = row.wkn
        }
        return result;
    }
}
