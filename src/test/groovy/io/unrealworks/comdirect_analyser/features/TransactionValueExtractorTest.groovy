package io.unrealworks.comdirect_analyser.features

import groovy.sql.Sql
import org.flywaydb.core.Flyway
import org.junit.jupiter.api.AfterEach
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.extension.ExtendWith
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.test.context.junit.jupiter.SpringExtension
import org.testcontainers.containers.MySQLContainer

import javax.sql.DataSource;

import static org.junit.jupiter.api.Assertions.*;

@ExtendWith(SpringExtension)
@SpringBootTest
@org.springframework.test.context.jdbc.Sql(scripts="TransactionValueExtractorTest.sql")
class TransactionValueExtractorTest {
    //def db = [url:'jdbc:hsqldb:mem:testDB', user:'sa', password:'', driver:'org.hsqldb.jdbc.JDBCDriver']
    @Autowired
    private DataSource dataSource
    @Autowired
    private Flyway flyway;
    private Sql sql


    /*private static final MySQLContainer mySQLContainer = new MySQLContainer()
            .withDatabaseName("comdirect_analyser")
            //.withExposedPorts(3306)
            .withEnv("MYSQL_ROOT_HOST", "%")
            .withEnv('MYSQL_ROOT_PASSWORD', 'test')
            .withEnv('MYSQL_ALLOW_EMPTY_PASSWORD', 'yes')
            .withCommand(" --default-authentication-plugin=mysql_native_password")
*/
    @BeforeEach
    void beforeEach(){
        this.sql = Sql.newInstance(dataSource)
    }

    @Test
    void compute() {
        def ftt = new TransactionCostsValueExtractor(sql)
        def result = ftt.compute("")
    }

    @AfterEach
    void afterEach(){
        flyway.clean()
    }
}