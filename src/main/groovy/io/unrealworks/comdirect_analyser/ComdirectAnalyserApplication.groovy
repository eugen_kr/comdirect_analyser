package io.unrealworks.comdirect_analyser

import io.unrealworks.comdirect_analyser.features.Feature
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import org.springframework.boot.ApplicationArguments
import org.springframework.boot.ApplicationRunner
import org.springframework.boot.SpringApplication
import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.context.ApplicationContext

@SpringBootApplication
class ComdirectAnalyserApplication implements ApplicationRunner {
    private final static Logger log = LoggerFactory.getLogger(ComdirectAnalyserApplication)

    static void main(String[] args) {
        SpringApplication.run(ComdirectAnalyserApplication, args)
    }

    ComdirectAnalyserApplication(ApplicationContext applicationContext) {
        this.applicationContext = applicationContext
    }

    private final ApplicationContext applicationContext;

    @Override
    void run(ApplicationArguments args) throws Exception {
        log.debug("{}", args);
        if (args.sourceArgs.length > 0) {
            log.debug("{}", args.sourceArgs);

            def filesCounter = this.applicationContext.getBean("files-counter")
            def numOfFiles = filesCounter.compute(args.sourceArgs[0])
            log.info("found {} files", numOfFiles)
            if (numOfFiles > 0) {
                /*log.info("starting raw data extraction ...")
                this.applicationContext.getBean("raw-data-extractor").compute(args.sourceArgs[0])

                log.info("finish raw data extraction... extraction transaction execution date")
                this.applicationContext.getBean("execution-date-extractor").compute('');

                log.info("finished execution date extraction ... starting extracting transaction type")
                this.applicationContext.getBean("transaction-type-extractor").compute('');

                log.info("finish type extraction .... starting shares info extraction")
                this.applicationContext.getBean("shares-info-extractor").compute('');

                log.info("... starting extracting transaction costs ")
                this.applicationContext.getBean('transaction-costs-value-extractor').compute('')
                /*/
                log.info("finished shares base info extraction .... starting extracting transaction value")
                this.applicationContext.getBean("transaction-value-extractor").compute('');

                log.info("...extracting final expense")
                this.applicationContext.getBean('final-transaction-value-extractor').compute('')
            }
        } else {
            log.warn("no path arguments found!");
        }
    }
}
