package io.unrealworks.comdirect_analyser.features

import groovy.sql.Sql
import org.apache.pdfbox.pdmodel.PDDocument
import org.apache.pdfbox.text.PDFTextStripper
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import org.springframework.stereotype.Component

@Component("raw-data-extractor")
class RawDataExtractor implements Feature<String, Integer> {
    private final static Logger log = LoggerFactory.getLogger(RawDataExtractor);

    private final Sql sql;

    RawDataExtractor(Sql sql) {
        this.sql = sql
    }

    @Override
    Integer compute(final String param) {
        final File baseDir = new File(param);
        if (countFilesInDB() == baseDir.listFiles().length) {
            return 0
        }

        int id = getHighestId()
        baseDir.listFiles().each(file -> {
            log.debug("extracting {}", file.name)
            PDDocument doc = PDDocument.load(file)
            def text = new PDFTextStripper().getText(doc)

            def stmt = 'insert into raw_data(id, file_name, content) values (?,?,?)'
            def params = [id, file.name, text];
            sql.executeInsert stmt, params
            doc.close()
            id++;
        })

        return baseDir.listFiles().length
    }

    private Integer getHighestId() {
        def firstRow = this.sql.firstRow('SELECT id FROM raw_data ORDER BY id DESC LIMIT 0, 1')
        log.debug("last id is {}", firstRow?.id)

        return firstRow != null ? firstRow.id + 1 : 0;
    }

    private Integer countFilesInDB() {
        return this.sql.firstRow('select count(*) as count from raw_data').count;
    }
}
